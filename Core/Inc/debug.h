#ifndef _DEBUG_H
#define _DEBUG_H

/**
* @name Debug print 
* @{
*/
#define PRINT_DEBUG_ENABLE		1		/* 打印调试信息 */
#define PRINT_ERR_ENABLE			1	  /* 打印错误信息 */
#define PRINT_INFO_ENABLE			1		/* 打印个人信息 */


#if PRINT_DEBUG_ENABLE
#define LOG_D(fmt, args...)  do{(rt_kprintf("\033[0;32;33m [DEB] "), rt_kprintf(fmt, ##args));rt_kprintf("\033[0m");}while(0)
#else
#define LOG_D(fmt, args...)	     
#endif

#if PRINT_ERR_ENABLE
#define LOG_E(fmt, args...) 	 do{(rt_kprintf("\033[0;32;31m [ERR] "), rt_kprintf(fmt, ##args));rt_kprintf("\033[0m");}while(0)     
#else
#define LOG_E(fmt, args...)	       
#endif

#if PRINT_INFO_ENABLE
#define LOG_I(fmt, args...) 	 do{(rt_kprintf("\033[0;32;32m [INF] "), rt_kprintf(fmt, ##args));rt_kprintf("\033[0m");}while(0)     
#else
#define LOG_I(fmt, args...)	       
#endif

/**@} */
	
//针对不同的编译器调用不同的stdint.h文件
#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
    #include <stdint.h>
#endif

/* 断言 Assert */
#define AssertCalled(char,int) 	rt_kprintf("\n\033[0;32;31m Error:%s,%d \033[0m \r\n",char,int)
#define ASSERT(x)   if((x)==0)  AssertCalled(__FILE__,__LINE__)
  
typedef enum 
{
	ASSERT_ERR = 0,								/* 错误 */
	ASSERT_SUCCESS = !ASSERT_ERR	/* 正确 */
} Assert_ErrorStatus;

typedef enum 
{
	FALSE = 0,		/* 假 */
	TRUE = !FALSE	/* 真 */
}ResultStatus;



#endif /* __DEBUG_H */

